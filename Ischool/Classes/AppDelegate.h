//
//  AppDelegate.h
//  Ischool
//
//  Created by 张灿 on 14/11/4.
//  Copyright (c) 2014年 张灿. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

