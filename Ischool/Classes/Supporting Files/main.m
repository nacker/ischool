//
//  main.m
//  Ischool
//
//  Created by 张灿 on 14/11/4.
//  Copyright (c) 2014年 张灿. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
